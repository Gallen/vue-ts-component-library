module.exports = {
  pages: {
    index: {
      entry: 'examples/main.ts',
      template: 'examples/index.html',
      filename: 'index.html',
    },
  },
  // 扩展 webpack 配置
  chainWebpack: (config) => {
    // 没有任何具名导出并直接暴露默认导出
    config.output.libraryExport('default');
    config.externals = {
      vue: 'Vue',
    };
  },
  css: { extract: false },
};
